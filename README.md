
This is project runs the conversion of the F-Droid/Fastlane app store
descriptions in [fdroiddata](https://gitlab.com/fdroid/fdroiddata)
into XLIFF format for
[Weblate](https://hosted.weblate.org/projects/f-droid/fdroiddata).

The update process is roughly like this:

0. pull in latest commits into local copy of _fdroiddata_
1. `./tools/make-summary-translatable.py` in _fdroiddata_ to make more summaries translatable
2. click Manage --> Commit in https://hosted.weblate.org/projects/f-droid/fdroiddata
3. switch back to this repo, then `git pull --ff-only weblate master` to get latest commits
4. `./convert-xliff-to-fdroiddata.py` to sync translations to _fdroiddata_
5. commit those updates in _fdroiddata_ and push
6. `./grab-from-fdroiddata.py` to get source string updates in this project
7. commit those here
8. `./tools/sanitize.sh` and commit those changes
9. `git push origin master`


Ideally, there would be a good tool for merging new source strings
into the translation files like _gettext_ has.  The very latest version
`pomerge` from _translate-toolkit_ supports XLIFF, but I couldn't get
it going.  KDE's _Lokalize_ also has XLIFF support.

## Translation

Everything on this website can be translated.  See
[Translation and Localization](https://f-droid.org/docs/Translation_and_Localization)
for more info.  The [staging site](https://staging.f-droid.org)
includes all translations, whether they are complete or not.

[![fdroiddata status](https://hosted.weblate.org/widgets/f-droid/-/fdroiddata/multi-auto.svg)](https://hosted.weblate.org/engage/f-droid/?utm_source=widget)
