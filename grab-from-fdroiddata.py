#!/usr/bin/env python3

import glob
import os
import re
import sys
import xml.sax.saxutils

from fdroidserver import common
from fdroidserver import index

try:
    from lxml import etree as ET
    print("running with lxml.etree")
except ImportError:
    import xml.etree.cElementTree as ET


def find_trans_unit(body, trans_unit_id):
    for trans_unit in body.findall('trans-unit'):
        if trans_unit.attrib['id'] == trans_unit_id:
            return trans_unit


config = dict()
config['jarsigner'] = 'jarsigner'
common.config = config
index.config = config
url = 'https://ftp.fau.de/fdroid/repo?fingerprint=43238D512C1E5EB2D6569F4A3AFBF5523418B82E0A3ED1552770ABB9A9C9CCAB'
indexdata, etag = index.download_repo_index(url)
indexPackageNames = indexdata['packages'].keys()

for d in sorted(glob.glob('../fdroiddata/metadata/[A-Za-z]*/[a-z][a-z]*')):
    if not os.path.isdir(d):
        continue
    segments = d.split('/')
    packageName = segments[3]
    if packageName in ['org.smssecure.smssecure', 'info.guardianproject.checkey',
                       'com.controlloid',
                       'info.guardianproject.ripple', 'org.kontalk', 'ru.hyst329.openfool']:
        continue  # moved to upstream source repo
    if packageName not in indexPackageNames:
        continue  # not in index
    locale = segments[4]
    if locale == 'signatures':
        continue
    langfile = os.path.join('localizations', locale + '.xliff')

    if not os.path.exists(langfile):
        with open(langfile, 'w') as fp:
            fp.write("""<?xml version="1.0" encoding="UTF-8"?>
<xliff version="1.2">
  <file datatype="plaintext" original="en-US.xliff" source-language="en-US">
    <header>
      <note from="developer">F-Droid, Google Play and other app stores place strict character limits on the various text fields. The title is 30 characters. The short description is 80 characters. The "What's New" is 500 characters. The rest of the strings, which all go together in the description, are limited to 4000 characters.</note>
    </header>
    <body>
    </body>
  </file>
</xliff>
""")
    print(packageName, '\tlangfile\t', langfile)
    parser = ET.XMLParser(remove_blank_text=True)
    tree = ET.parse(langfile, parser)
    treeroot = tree.getroot()
    body = treeroot.find('file').find('body')

    for root, dirs, files in os.walk(d):
        for f in sorted(files):
            name, ext = os.path.splitext(f)
            if ext not in ('.txt', '.yml'):
                continue
            with open(os.path.join(root, f).replace('/' + locale + '/', '/en-US/')) as readfp:
                sourcetext = readfp.read().strip()
            with open(os.path.join(root, f)) as readfp:
                targettext = readfp.read().strip()
            if name == 'name':
                trans_unit_id = packageName + '-' + name
                trans_unit = find_trans_unit(body, trans_unit_id)
                if trans_unit is not None:
                    note = trans_unit.find('note')
                    source = trans_unit.find('source')
                    target = trans_unit.find('target')
                    if target is None:
                        target = ET.SubElement(trans_unit, 'target')
                else:
                    trans_unit = ET.SubElement(body, 'trans-unit',
                                               {'id': trans_unit_id,
                                                'size-unit': 'char',
                                                'maxwidth': '30'})
                    note = ET.SubElement(trans_unit, 'note', {'from': 'developer'})
                    source = ET.SubElement(trans_unit, 'source')
                    target = ET.SubElement(trans_unit, 'target')
                note.text = 'This is limited by app stores to 30 characters'
                source.text = sourcetext
                target.text = targettext
            elif name == 'summary':
                trans_unit_id = packageName + '-' + name
                trans_unit = find_trans_unit(body, trans_unit_id)
                if trans_unit is not None:
                    note = trans_unit.find('note')
                    source = trans_unit.find('source')
                    target = trans_unit.find('target')
                    if target is None:
                        target = ET.SubElement(trans_unit, 'target')
                else:
                    trans_unit = ET.SubElement(body, 'trans-unit',
                                               {'id': trans_unit_id,
                                                'size-unit': 'char',
                                                'maxwidth': '80'})
                    note = ET.SubElement(trans_unit, 'note', {'from': 'developer'})
                    source = ET.SubElement(trans_unit, 'source')
                    target = ET.SubElement(trans_unit, 'target')
                note.text = 'Read about the app here: https://f-droid.org/packages/' + packageName
                source.text = sourcetext.replace('\n', ' ')
                target.text = targettext.replace('\n', ' ')
            elif name == 'description':
                pass  # descriptions are too hard to manage, don't auto-sync
            elif re.match(r'-?[0-9]+', name):
                trans_unit_id = packageName + '-changelog-' + name
                trans_unit = find_trans_unit(body, trans_unit_id)
                if trans_unit is not None:
                    note = trans_unit.find('note')
                    source = trans_unit.find('source')
                    target = trans_unit.find('target')
                    if target is None:
                        target = ET.SubElement(trans_unit, 'target')
                else:
                    trans_unit = ET.SubElement(body, 'trans-unit',
                                               {'id': trans_unit_id,
                                                'size-unit': 'char',
                                                'maxwidth': '500'})
                    note = ET.SubElement(trans_unit, 'note', {'from': 'developer'})
                    source = ET.SubElement(trans_unit, 'source')
                    target = ET.SubElement(trans_unit, 'target')
                note.text = 'This is limited by app stores to 500 characters'
                source.text = sourcetext.replace('\n', '\\n')
                target.text = targettext.replace('\n', '\\n')
            else:
                print('bad filename, needs to be renamed or deleted:', f)
                sys.exit(1)

    with open(langfile, 'wb') as fp:
        fp.write(ET.tostring(treeroot, encoding="UTF-8", xml_declaration=True, pretty_print=True))

# remove all packages that are not currently in the index
parser = ET.XMLParser(remove_blank_text=True)
tree = ET.parse('localizations/en-US.xliff', parser)
treeroot = tree.getroot()
body = treeroot.find('file').find('body')
for trans_unit in body.findall('trans-unit'):
    dash = trans_unit.attrib['id'].find('-')
    packageName = trans_unit.attrib['id'][:dash]
    if packageName not in indexPackageNames:
        print('DELETE, NOT IN INDEX:', trans_unit.attrib['id'])
        trans_unit.getparent().remove(trans_unit)
with open('localizations/en-US.xliff', 'wb') as fp:
    fp.write(ET.tostring(treeroot, encoding="UTF-8", xml_declaration=True, pretty_print=True))

# now make sure all source strings are present

for f in sorted(glob.glob('localizations/*.xliff')):
    print('add untranslated strings', f)
    if f == 'localizations/en-US.xliff':
        continue
    parser = ET.XMLParser(remove_blank_text=True)
    tree = ET.parse(f, parser)
    treeroot = tree.getroot()
    body = treeroot.find('file').find('body')

    sourceparser = ET.XMLParser(remove_blank_text=True)
    sourcetreeroot = ET.parse('localizations/en-US.xliff', sourceparser).getroot()
    sourcebody = sourcetreeroot.find('file').find('body')
    for source_trans_unit in sourcebody.findall('trans-unit'):
        target = source_trans_unit.find('target')
        if target is not None:
            source_trans_unit.remove(target)
        print('checking for', source_trans_unit.attrib['id'])
        found = False
        for trans_unit in body.findall('trans-unit'):
            dash = trans_unit.attrib['id'].find('-')
            packageName = trans_unit.attrib['id'][:dash]
            if packageName not in indexPackageNames:
                print('DELETE, NOT IN INDEX:', trans_unit.attrib['id'])
                trans_unit.getparent().remove(trans_unit)
                continue
            if trans_unit.attrib['id'].endswith('-summary'):
                note = trans_unit.find('note')
                note.text = 'Read about the app here: https://f-droid.org/packages/' + trans_unit.attrib['id'][:-8]
            if source_trans_unit.attrib['id'] == trans_unit.attrib['id']:
                print('\tfound', source_trans_unit.attrib['id'], trans_unit.attrib['id'])
                trans_unit.find('source').text = source_trans_unit.find('source').text
                found = True

        if not found:
            print('\tNOT FOUND:', source_trans_unit.attrib['id'])
            body.append(source_trans_unit)

        #print(source_trans_unit.tag, source_trans_unit.attrib['id'])
        #print(source_trans_unit.find('source').text)

    with open(f, 'wb') as fp:
        fp.write(ET.tostring(treeroot, encoding="UTF-8", xml_declaration=True, pretty_print=True))
